# Taken from matplotlib.org

import numpy as np
import matplotlib.pyplot as plt

n = 4000 #changed to 4000!!
X = np.random.normal(0,10,n) #changed 1 to 10
Y = np.random.normal(0,1,n)

plt.scatter(X,Y)
plt.savefig("scatter_plot.png") # this line saves the file as a png.
plt.show()
