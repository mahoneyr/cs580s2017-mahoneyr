import matplotlib.pyplot as plt
# taken from http://matplotlib.org/users/pyplot_tutorial.html
plt.plot([1,2,3,4,5,6], [1,4,9,16, 24, 70], 'g^') #changed circles to triangles and added 2 more points
plt.axis([0, 15, 0, 50]) #change dimesnion tp 15 and 50
plt.savefig("dotPlot1_plot.png") # this line saves the file as a png.
plt.show() # this line shows the graphic in a window.
