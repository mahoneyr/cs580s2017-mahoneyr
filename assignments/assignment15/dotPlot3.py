import numpy as np
import matplotlib.pyplot as plt
#taken from: http://matplotlib.org/users/pyplot_tutorial.html

def f(t):
    return np.exp(-t) * np.cos(2*np.pi*t)

t1 = np.arange(0.0, 7.0, 0.1) #changed 5.0 to 7.0
t2 = np.arange(0.0, 7.0, 0.02)

plt.figure(1)
plt.subplot(211)
plt.plot(t1, f(t1), '4', t2, f(t2), 'k') #bo to 4

plt.subplot(212)
plt.plot(t2, np.cos(2*np.pi*t2), 'r--')

plt.savefig("dotPlot3_plot.png") # this line saves the file as a png.
plt.show() # this line shows the graphic in a window.
