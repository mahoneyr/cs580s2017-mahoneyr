import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

days, prices = np.loadtxt("google-prices.csv", delimiter=',',unpack=True,
        converters={ 0: mdates.strpdate2num('%Y-%m-%d')})

plt.plot_date(x=days, y=prices)

#plt.plot_date(x=days, y=prices, fmt="r-")

plt.title("Historical Closing Prices of Goooooooogle") #changed spelling
plt.ylabel("Closing Price")
plt.grid(False) #removed grid
plt.savefig("linegraph_plot.png") # this line saves the file as a png.
plt.show() # this line shows the graphic in a window.
