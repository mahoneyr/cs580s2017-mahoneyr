import numpy as np
import matplotlib.pyplot as plt
#taken from: http://matplotlib.org/users/pyplot_tutorial.html

# evenly sampled time at 200ms intervals
t = np.arange(0., 5., 0.2)

# red dashes, blue squares and green triangles
plt.plot(t, t, 'p', t, t**2, 'bs', t, t**3, '2') #chnaged shape to tri shape and pentagon
plt.savefig("dotPlot2_plot.png") # this line saves the file as a png.
plt.show() # this line shows the graphic in a window.
